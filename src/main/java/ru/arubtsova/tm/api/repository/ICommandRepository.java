package ru.arubtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> getCommandList();

    @NotNull
    List<AbstractCommand> getCommandNames();

}
