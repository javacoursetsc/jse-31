package ru.arubtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class CommandsListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "show application commands.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        System.out.println("Available commands:");
        for (@NotNull final AbstractCommand command : commands) {
            @Nullable final String name = command.name();
            if (!Optional.ofNullable(name).isPresent()) continue;
            System.out.println(name);
        }
    }

}