package ru.arubtsova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "log you out of the system.";
    }

    @Override
    public void execute() {
        System.out.println("Logout:");
        serviceLocator.getAuthService().logout();
        System.out.println("You have been successfully logged out");
    }

}
