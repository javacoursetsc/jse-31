package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "update a task by id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Task:");
        System.out.println("Enter Task Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Enter New Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Task Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Optional<Task> taskUpdate = serviceLocator.getTaskService().updateById(userId, id, name, description);
        Optional.ofNullable(taskUpdate).orElseThrow(TaskNotFoundException::new);
        System.out.println("Task was successfully updated");
    }

}
