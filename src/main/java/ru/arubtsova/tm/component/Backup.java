package ru.arubtsova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.service.IPropertyService;
import ru.arubtsova.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private static final String SAVE_COMMAND = "backup-save";

    @NotNull
    private static final String LOAD_COMMAND = "backup-load";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final IPropertyService propertyService;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.propertyService = propertyService;
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, propertyService.getBackupInterval(), TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void init() {
        load();
        start();
    }

    public void run() {
        bootstrap.parseCommand(SAVE_COMMAND);
    }

    public void load() {
        bootstrap.parseCommand(LOAD_COMMAND);
    }

}
