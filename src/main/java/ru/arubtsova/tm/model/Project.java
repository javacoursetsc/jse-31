package ru.arubtsova.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.entity.IWBS;

@NoArgsConstructor
public class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@NotNull String name) {
        this.name = name;
    }

}
