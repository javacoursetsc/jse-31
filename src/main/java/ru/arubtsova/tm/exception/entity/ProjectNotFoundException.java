package ru.arubtsova.tm.exception.entity;

import ru.arubtsova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
